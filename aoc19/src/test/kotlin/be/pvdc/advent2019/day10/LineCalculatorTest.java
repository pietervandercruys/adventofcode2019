package be.pvdc.advent2019.day10;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class LineCalculatorTest {

    @Parameterized.Parameters(name = "{index} => {0} {1}: {2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {c(0,0), c(1,0), Arrays.asList(c(0,0), c(1,0))},
                {c(0,0), c(2,0), Arrays.asList(c(0,0), c(1,0), c(2,0))},
                {c(0,0), c(5,0), Arrays.asList(c(0,0), c(1,0), c(2,0), c(3,0), c(4,0), c(5,0))},
                {c(0,0), c(0,5), Arrays.asList(c(0,0), c(0,1), c(0, 2), c(0,3), c(0,4), c(0,5))},
                {c(1,1), c(1,2), Arrays.asList(c(1,1), c(1,2))},
                {c(1,1), c(2,1), Arrays.asList(c(1,1), c(2,1))},
                {c(1,1), c(3,3), Arrays.asList(c(1,1), c(2,2), c(3,3))},
                {c(3,3), c(1,1), Arrays.asList(c(1,1), c(2,2), c(3,3))},
                {c(1,3), c(3,1), Arrays.asList(c(1,3), c(2,2), c(3,1))},
                {c(3,5), c(9,8), Arrays.asList(c(3,5), c(5,6), c(7,7), c(9,8))},
        });
    }

    private Coordinate c1;
    private Coordinate c2;
    private List<Collection> line;
    public LineCalculatorTest(Coordinate c1, Coordinate c2, List<Collection> line) {
        this.c1 = c1;
        this.c2 = c2;
        this.line = line;
    }

    @Before
    public void setUp() throws Exception {
        lineCalculator = new LineCalculator();
    }

    @Test
    public void test() {
        assertThat(new HashSet<>(lineCalculator.calculateLine(c1, c2))).isEqualTo(new HashSet<>(line));
    }

    private LineCalculator lineCalculator;


    private static Coordinate c(int x, int y) {
        return new Coordinate(x, y);
    }
}

