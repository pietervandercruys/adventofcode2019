package be.pvdc.advent2019.day10;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class SpaceIndexerTest {
    @Parameterized.Parameters(name = "{index}: {0} => {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"", Arrays.asList()},
            {"      ", Arrays.asList()},
            {"#", Arrays.asList(new Coordinate(0,0))},
            {" #", Arrays.asList(new Coordinate(1,0))},
            {" \n" +
             "#", Arrays.asList(new Coordinate(0,1))},
            {"# #\n" +
             " # ", Arrays.asList(new Coordinate(0,0), new Coordinate(2,0), new Coordinate(1,1))},
        });
    }
    private String space;
    private List<Coordinate> spaceIndex;

    public SpaceIndexerTest(String space, List<Coordinate> spaceIndex) {
        this.space = space;
        this.spaceIndex = spaceIndex;
    }

    private SpaceIndexer spaceIndexer;

    @Before
    public void setUp() throws Exception {
        spaceIndexer = new SpaceIndexer();
    }

    @Test
    public void test() {
        assertThat(spaceIndexer.index(space)).isEqualTo(spaceIndex);
    }
}
