package be.pvdc.advent2019.day10;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class SpaceIndexer {
    public SpaceIndexer() {
    }

    public List<Coordinate> index(String space) {
        List<Coordinate> index = new ArrayList<>();
        AtomicInteger x = new AtomicInteger();
        AtomicInteger y = new AtomicInteger();
        space.chars()
                .forEach(c -> {
                    if(c == '#') {
                        index.add(new Coordinate(x.get(), y.get()));
                    }
                    if(c == '\n') {
                        x.set(0);
                        y.getAndIncrement();
                    }
                    else {
                    x.getAndIncrement();
                    }
                });
        return index;
    }
}
