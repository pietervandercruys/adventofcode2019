package be.pvdc.advent2019.day10;


import kotlin.text.Charsets;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Day10 {
    public static void main(String[] args) throws IOException {
        new Day10().day10();
    }

    LineCalculator lineCalculator = new LineCalculator();

    private void day10() throws IOException {
        String space = readInput();

        List<Coordinate> asteroids = new SpaceIndexer().index(space);

        Long max = asteroids.stream()
                .map(a -> countDirectLinesToOtherAsteroids(asteroids, a))
                .reduce(Long::max)
                .orElse(0L);
        System.out.println("max = " + max);

        Coordinate monitoringStation = asteroids.stream()
                .filter(a -> countDirectLinesToOtherAsteroids(asteroids, a).equals(max))
                .findFirst()
                .orElse(null);
        System.out.println("monitoringStation = " + monitoringStation);

        List<WayPoint> waypoints = asteroids.stream()
                .filter(a -> ! a.equals(monitoringStation))
                .map(a -> monitoringStation.toWayPoint(a))
                .sorted()
                .collect(Collectors.toList());

        int counter = 0;
        int idx = 0;
        WayPoint vaporized = null;
        while (counter <= waypoints.size())
        {
            WayPoint toVaporize = waypoints.get(idx++);
            if(idx >= waypoints.size())
                idx = 0;

            if(toVaporize.getVaporized())
                continue;
            if(vaporized != null && vaporized.getAngel() == toVaporize.getAngel())
                continue;

            toVaporize.vaporize();
            vaporized = toVaporize;
            counter++;

            System.out.println(counter+" vaporized = " + vaporized);
        }
    }

    @NotNull
    private Long countDirectLinesToOtherAsteroids(List<Coordinate> asteroids, Coordinate a) {
        List<List<Coordinate>> allLinesToAllOtherAsteroids = findAllLinesToAllOtherAsteroids(a, asteroids);
        return countDirectLines(allLinesToAllOtherAsteroids, asteroids);
    }

    private long countDirectLines(List<List<Coordinate>> lines, List<Coordinate> asteroids) {
        long nrOfDirectLines = lines.stream()
                .filter(l -> isNotInterrupted(l, asteroids))
                .count();
        return nrOfDirectLines;
    }

    private boolean isNotInterrupted(List<Coordinate> l, List<Coordinate> asteroids) {
        boolean direct = asteroids.stream()
                .noneMatch(l::contains);
        return direct;
    }

    private List<List<Coordinate>> findAllLinesToAllOtherAsteroids(Coordinate asteroid, List<Coordinate> asteroids) {
        List<List<Coordinate>> allLinesForAsteroid = asteroids.stream()
                .filter(a -> !a.equals(asteroid))
                .map(a -> {
                    List<Coordinate> coordinates = lineCalculator.calculateLine(asteroid, a);
                    return removeEndpoints(coordinates, asteroid, a);
                })
                .collect(Collectors.toList());
        return allLinesForAsteroid;
    }

    private List<Coordinate> removeEndpoints(List<Coordinate> coordinates, Coordinate asteroid1, Coordinate asteroid2) {
        return coordinates.stream()
                .filter(a -> !a.equals(asteroid1))
                .filter(a -> !a.equals(asteroid2))
                .collect(Collectors.toList());
    }


    private String readInput() throws IOException {
        return new String(Files.readAllBytes(Paths.get("./aoc19/src/main/kotlin/be/pvdc/advent2019/day10/day10.input.txt")), Charsets.UTF_8);
    }

    private static Coordinate c(int x, int y) {
        return new Coordinate(x, y);
    }
}
