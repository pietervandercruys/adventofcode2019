package be.pvdc.advent2019.day10;

import java.util.ArrayList;
import java.util.List;

public class LineCalculator {

    public List<Coordinate> calculateLine(Coordinate c1, Coordinate c2) {
        List<Coordinate> line = new ArrayList<>();
            Coordinate norm = findNorm(c1, c2);

            int step = -1;
            do {
                step++;
                line.add(c1.plus(norm.multiply(step)));
            }
            while (!c1.plus(norm.multiply(step)).equals(c2));

        return line;
    }

    private Coordinate findNorm(Coordinate c1, Coordinate c2) {
        return c2.minus(c1).nomalize();
    }

    private Coordinate c(int x, int y) {
        return new Coordinate(x, y);
    }
}
