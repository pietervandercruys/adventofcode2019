package be.pvdc.advent2019.day10

import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

class Coordinate(var x:Int, var y:Int) {
    override fun toString(): String {
        return "($x,$y)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Coordinate

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }

    fun plus(other: Coordinate): Coordinate {
        return Coordinate(this.x + other.x, this.y + other.y);
    }

    fun minus(other: Coordinate): Coordinate {
        return Coordinate(this.x - other.x, this.y - other.y);
    }

    fun multiply(times: Int): Coordinate {
        return Coordinate(this.x * times, this.y * times)
    }

    fun devide(times: Int): Coordinate {
        return Coordinate(this.x / times, this.y / times);
    }

    fun nomalize(): Coordinate {
        if(this.x == 0 || this.y == 0)
            return this.devide(maxOf(abs(this.x), abs(this.y)));
        else
            return this.devide(gcdBrute(this.x, this.y))
    }

    fun distance(other: Coordinate): Double {
        return sqrt ((other.x - this.x).toDouble().pow(2) + (other.y - this.y).toDouble().pow(2))
    }

    fun angleOfYAxis(other: Coordinate): Double {
        val w = other.x - this.x
        val h = other.y - this.y
        var atan = Math.atan(h.toDouble()/w.toDouble()) / Math.PI * 180
        if (w < 0)
            atan += 270
        else {
            atan += 90
        }

        return atan % 360;
    }

    fun toWayPoint(other: Coordinate): WayPoint {
        return WayPoint(other, this.distance(other), this.angleOfYAxis(other))
    }

    private fun gcdBrute(n1: Int, n2: Int): Int {
        val absN1 = abs(n1)
        val absN2 = abs(n2)
        var gcd = 1
        var i = 1
        while (i <= absN1 && i <= absN2) {
            if (absN1 % i == 0 && absN2 % i == 0) {
                gcd = i
            }
            i++
        }
        return gcd
    }
}