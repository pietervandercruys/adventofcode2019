package be.pvdc.advent2019.day10

class WayPoint(var asteroid: Coordinate, var distance: Double, var angel: Double) : Comparable<WayPoint>
{
    var vaporized = false
        get() = field
        set(vapo) {
            field = vapo
        }
    fun vaporize(){
        vaporized = true
    }

    override fun toString(): String {
        return "WayPoint(asteroid=$asteroid, distance=$distance, angel=$angel)"
    }

    override fun compareTo(other: WayPoint): Int {
        var comp = (this.angel - other.angel);
        if(comp == 0.0) {
            comp = this.distance - other.distance
        }
        return (comp * 100000).toInt()
    }


}