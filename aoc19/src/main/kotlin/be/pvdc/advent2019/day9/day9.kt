package be.pvdc.advent2019.day9

import java.io.File

fun main() {
    val computer = IntcodeComputer("Day9", readInput(), longArrayOf(2))
    computer.run()
    computer.output.stream().forEach { o -> print(o.toString()+" ") }
}

private fun readInput(): String {
    return File("./aoc19/src/main/kotlin/be/pvdc/advent2019/day9/day9.input.txt").readText(Charsets.UTF_8)
//    return "1102,34915192,34915192,7,4,7,99,0"
}