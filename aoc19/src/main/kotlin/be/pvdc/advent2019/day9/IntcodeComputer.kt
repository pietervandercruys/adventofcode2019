package be.pvdc.advent2019.day9

class IntcodeComputer(var name: String, var textInput: String, var programInput: LongArray) {
    val memory = 2048
    var relativeBase = 0
    val output = ArrayList<Long>()
    var inputComputer: IntcodeComputer? = null
        get() = field
        set(inputComputer) {
        field = inputComputer
    }

    fun run(): ArrayList<Long> {
        output.clear()
        val input = textInput.split(",").map { it.toLong() }.toLongArray()
        val programm = LongArray(memory)
        input.copyInto(programm)
        process(programm)
        return output
    }

    private fun process(input: LongArray) {
        var state = State(input, 0)
        while (! state.final()) {
            state = process(state)
        }
    }

    fun hasOutput(): Boolean {
        return output.size > 0
    }

    fun getOutput(): Long {
        return output.removeAt(0)
    }

    var programInputPointer = 0
    private fun readInput(input: LongArray, pos: Int): State {
        val writePos = getParPos(input, pos, 1)

        if(programInputPointer < programInput.size)
        {
            input[writePos] = programInput[programInputPointer++]
        }
        else {
            while (!inputComputer!!.hasOutput()) {
                Thread.sleep(10)
            }
            input[writePos] = inputComputer!!.getOutput()
        }
        return State(input, pos+2)
    }

    private fun output(input: LongArray, pos: Int): State {
        val var1 = getPar1(input, pos)
//        println(name+" Output "+var1)
        output.add(var1)
        return State(input, pos+2)
    }

    private fun process(state: State): State {
        val input = state.programm
        val pos = state.pos
        val fullOpcode = input[pos]
        val opcode = getOpcode(input, pos)
        if (opcode == 1)
            return add(input, pos)
        if(opcode == 2)
            return multiply(input, pos)
        if(opcode == 3)
            return readInput(input, pos)
        if(opcode == 4)
            return output(input, pos)
        if(opcode == 5)
            return jumpIfTrue(input, pos)
        if(opcode == 6)
            return jumpIfFalse(input, pos)
        if(opcode == 7)
            return lessThan(input, pos)
        if(opcode == 8)
            return equals(input, pos)
        if(opcode == 9)
            return adjustRelativeBase(input, pos)
        return State(input, pos);
    }

    private fun adjustRelativeBase(input: LongArray, pos: Int): State {
        val var1 = getPar1(input, pos)
        relativeBase += var1.toInt()
        return State(input, pos +2)
    }

    private fun jumpIfTrue(input: LongArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        if(var1 != 0L)
            return State(input, var2.toInt())
        else
            return State(input, pos+3)
    }

    private fun jumpIfFalse(input: LongArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        if(var1 == 0L)
            return State(input, var2.toInt())
        else
            return State(input, pos+3)
    }

    private fun lessThan(input: LongArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val var3 = input[pos+3]
        if(var1 < var2)
            input[getParPos(input, pos, 3)] = 1
        else
            input[getParPos(input, pos, 3)] = 0
        return State(input, pos+4)
    }

    private fun equals(input: LongArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val var3 = input[pos+3]
        if(var1 == var2)
            input[getParPos(input, pos, 3)] = 1
        else
            input[getParPos(input, pos, 3)] = 0
        return State(input, pos+4)
    }

    private fun add(input: LongArray, pos: Int): State {
        val opcode = input[pos].toInt()
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val result = var1 + var2
        writeResult(opcode, input, pos, result)
        return State(input, pos+4)
    }

    private fun multiply(input: LongArray, pos: Int): State {
        val opcode = input[pos].toInt()
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val result = var1 * var2
        writeResult(opcode, input, pos, result)
        return State(input, pos+4)
    }

    private fun writeResult(opcode: Int, input: LongArray, pos: Int, result: Long) {
        if (par3ImmediateMode(opcode))
            input[pos + 3] = result
        else if(parRelativeMode(opcode, 3))
            input[relativeBase + input[pos + 3].toInt()] = result
        else
            input[input[pos + 3].toInt()] = result
    }

    fun getOpcode(input: LongArray, pos: Int): Int {
        return input[pos].rem(10).toInt()
    }

    fun getPar1(input: LongArray, pos: Int): Long {
        return getPar(input, pos, 1)
    }

    fun getPar2(input: LongArray, pos: Int): Long {
        return getPar(input, pos, 2)
    }

    fun getPar3(input: LongArray, pos: Int): Long {
        return getPar(input, pos, 3)
    }

    fun getPar(input: LongArray, pos: Int, parNr: Int): Long {
        return input[getParPos(input, pos, parNr)]
    }

    fun getParPos(input: LongArray, pos: Int, parNr: Int): Int {
        val opcode = input[pos].toInt()
        if(parImmediateMode(opcode, parNr))
            return pos+parNr
        if(parRelativeMode(opcode, parNr))
            return relativeBase + input[pos+parNr].toInt()
        else
            return input[pos+parNr].toInt()
    }

    private fun par1ImmediateMode(opcode: Int): Boolean {
        return parImmediateMode(opcode, 1)
    }

    private fun par2ImmediateMode(opcode: Int): Boolean {
        return parImmediateMode(opcode, 2)
    }

    private fun par3ImmediateMode(opcode: Int): Boolean {
        return parImmediateMode(opcode, 3)
    }

    private fun parImmediateMode(opcode: Int, par: Int): Boolean {
        return parMode(opcode, par, '1')
    }

    private fun parRelativeMode(opcode: Int, par: Int): Boolean {
        return parMode(opcode, par, '2')
    }

    private fun parMode(opcode: Int, par: Int, mode: Char): Boolean {
        val opcodeString = opcode.toString()
        if(opcodeString.length < 2+par)
            return false
        val c = opcodeString[opcodeString.length - (2 + par)]
        if(c.equals(mode))
            return true
        return false
    }
}

private class State(val programm: LongArray, val pos: Int) {
    fun final(): Boolean {
        return programm[pos] == 99L
    }
}