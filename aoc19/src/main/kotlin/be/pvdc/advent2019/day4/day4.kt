package be.pvdc.advent2019.day4

fun main() {
    part1()
    part2()
}

private fun part1() {
    if (  meetPasswordCriteriaPart1(111111.toString()))
        println("part1 => 111111 OK")
    if (! meetPasswordCriteriaPart1(223450.toString()))
        println("part1 => 223450 OK")
    if (! meetPasswordCriteriaPart1(123789.toString()))
        println("part1 => 123789 OK")

    var countPart1 = 0;
    for (password in 245182..790572) {
        if (meetPasswordCriteriaPart1(password.toString())) {
//            println(password)
            countPart1++
        }
    }
    println()
    println("part1 = "+countPart1)
}

private fun part2() {
    if (  meetPasswordCriteriaPart2(112233.toString()))
        println("part2 => 112233 OK")
    else
        println("part2 => 112233 NOT OK")
    if (! meetPasswordCriteriaPart2(123444.toString()))
        println("part2 => 123444 OK")
    else
        println("part2 => 123444 NOT OK")
    if (! meetPasswordCriteriaPart2(777789.toString()))
        println("part2 => 777789 OK")
    else
        println("part2 => 777789 NOT OK")
    if (  meetPasswordCriteriaPart2(111122.toString()))
        println("part2 => 111122 OK")
    else
        println("part2 => 111122 NOT OK")

    var countPart2 = 0;
    for (password in 245182..790572) {
        if (meetPasswordCriteriaPart2(password.toString())) {
//            println(password)
            countPart2++
        }
    }
    println()
    println("part2 = "+countPart2)

}

private fun meetPasswordCriteriaPart1(number: String): Boolean {
    return isSixDigitNumber(number) &&
            twoAdjacentDigitsAreTheSame(number) &&
            theDigitsNeverDecrease(number);
}

private fun meetPasswordCriteriaPart2(number: String): Boolean {
    return isSixDigitNumber(number) &&
            twoAndNotMoreAdjacentDigitsAreTheSame(number) &&
            theDigitsNeverDecrease(number);
}

private fun isSixDigitNumber(number: String): Boolean {
    return number.length == 6;
}

private fun twoAdjacentDigitsAreTheSame(number: String): Boolean {
    for (x in 0..4) {
        if (digitValue(number, x) == digitValue(number, x + 1))
            return true;
    }
    return false;
}

private fun twoAndNotMoreAdjacentDigitsAreTheSame(number: String): Boolean {
    var skipIt = false
    var toSkip = -1
    for (x in 0..4) {
        if (skipIt) {
            if(allSameDigitsSkipped(number, x, toSkip)) {
                skipIt = false
                toSkip = -1
            }
            else {
                continue
            }
        }
        if (twoAdjacentDigits(number, x)) {
            if (atTheEndOfTheSequence(x))
                return true
            if (thirthDigitDifferent(number, x))
                return true
            else {
                skipIt = true
                toSkip = digitValue(number, x)
            }
        }
    }
    return false;
}

private fun atTheEndOfTheSequence(x: Int) = x == 4

private fun thirthDigitDifferent(number: String, x: Int) = number[x + 2] != number[x]

private fun twoAdjacentDigits(number: String, x: Int) = digitValue(number, x) == digitValue(number, x + 1)

private fun allSameDigitsSkipped(number: String, x: Int, toSkip: Int) =
        digitValue(number, x) != toSkip

private fun theDigitsNeverDecrease(number: String): Boolean {
    for (x in 0..4) {
        if (digitValue(number, x) > digitValue(number, x + 1))
            return false;
    }
    return true;
}

private fun digitValue(number: String, x: Int) = number.substring(x, x+1).toInt()



