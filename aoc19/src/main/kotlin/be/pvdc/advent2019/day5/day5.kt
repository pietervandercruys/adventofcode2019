package be.pvdc.advent2019.day5



import java.io.File
import java.lang.Exception

val inputValue = 5

fun main() {
    val textInput = File("./aoc19/src/main/kotlin/be/pvdc/advent2019/day5/day5.input.txt").readText(Charsets.UTF_8)
    val input = textInput.split(",").map { it.toInt() }.toIntArray()

    process(input);
}

private fun process(input: IntArray) {
    var state = State(input, 0)
    while (! state.final()) {
        state = process(state)
    }
}

fun process(state: State): State {
    val input = state.programm
    val pos = state.pos
    val fullOpcode = input[pos]
    val opcode = getOpcode(input, pos)
    if (opcode == 1)
        return add(input, pos)
    if(opcode == 2)
        return multiply(input, pos)
    if(opcode == 3)
        return readInput(input, pos)
    if(opcode == 4)
        return output(input, pos)
    if(opcode == 5)
        return jumpIfTrue(input, pos)
    if(opcode == 6)
        return jumpIfFalse(input, pos)
    if(opcode == 7)
        return lessThan(input, pos)
    if(opcode == 8)
        return equals(input, pos)

    return State(input, pos);
}

fun jumpIfTrue(input: IntArray, pos: Int): State {
    val opcode = input[pos]
    val var1 = getPar1(input, pos)
    val var2 = getPar2(input, pos)
    if(var1 != 0)
        return State(input, var2)
    else
        return State(input, pos+3)
}

fun jumpIfFalse(input: IntArray, pos: Int): State {
    val opcode = input[pos]
    val var1 = getPar1(input, pos)
    val var2 = getPar2(input, pos)
    if(var1 == 0)
        return State(input, var2)
    else
        return State(input, pos+3)
}

fun lessThan(input: IntArray, pos: Int): State {
    val opcode = input[pos]
    val var1 = getPar1(input, pos)
    val var2 = getPar2(input, pos)
    val var3 = input[pos+3]
    if(var1 < var2)
        input[var3] = 1
    else
        input[var3] = 0
    return State(input, pos+4)
}

fun equals(input: IntArray, pos: Int): State {
    val opcode = input[pos]
    val var1 = getPar1(input, pos)
    val var2 = getPar2(input, pos)
    val var3 = input[pos+3]
    if(var1 == var2)
        input[var3] = 1
    else
        input[var3] = 0
    return State(input, pos+4)
}

fun add(input: IntArray, pos: Int): State {
    val opcode = input[pos]
    val var1 = getPar1(input, pos)
    val var2 = getPar2(input, pos)
    val result = var1 + var2
    writeResult(opcode, input, pos, result)
    return State(input, pos+4)
}

fun multiply(input: IntArray, pos: Int): State {
    val opcode = input[pos]
    val var1 = getPar1(input, pos)
    val var2 = getPar2(input, pos)
    val result = var1 * var2
    writeResult(opcode, input, pos, result)
    return State(input, pos+4)
}

fun readInput(input: IntArray, pos: Int): State {
    val var1 = input[pos+1]
    input[var1] = inputValue;
    return State(input, pos+2)
}

fun output(input: IntArray, pos: Int): State {
    val var1 = getPar1(input, pos)
    println(var1)
    return State(input, pos+2)
}

private fun writeResult(opcode: Int, input: IntArray, pos: Int, result: Int) {
    if (par3ImmediateMode(opcode))
        input[pos + 3] = result
    else
        input[input[pos + 3]] = result
}

fun getOpcode(input: IntArray, pos: Int): Int {
    return input[pos].rem(10)
}

fun getPar1(input: IntArray, pos: Int): Int {
    val opcode = input[pos]
    if(par1ImmediateMode(opcode))
        return input[pos + 1]
    else
        return input[input[pos+1]]
}

fun getPar2(input: IntArray, pos: Int): Int {
    val opcode = input[pos]
    if(par2ImmediateMode(opcode))
        return input[pos + 2]
    else
        return input[input[pos+2]]
}

fun getPar3(input: IntArray, pos: Int): Int {
    val opcode = input[pos]
    if(par3ImmediateMode(opcode))
        return input[pos + 3]
    else
        return input[input[pos+3]]
}

fun replace(input: IntArray, noun: Int, verb: Int): IntArray {
    input[1] = noun
    input[2] = verb
    return input
}

private fun par1ImmediateMode(opcode: Int): Boolean {
    return parImmadiateMode(opcode, 1)
}

private fun par2ImmediateMode(opcode: Int): Boolean {
    return parImmadiateMode(opcode, 2)
}

private fun par3ImmediateMode(opcode: Int): Boolean {
    return parImmadiateMode(opcode, 3)
}

private fun parImmadiateMode(opcode: Int, par: Int): Boolean {
    val opcodeString = opcode.toString()
    if(opcodeString.length < 2+par)
        return false
    if(opcodeString[opcodeString.length-(2+par)] == '1')
        return true

    return false
}

class State(val programm: IntArray, val pos: Int) {
    fun final(): Boolean {
        return programm[pos] == 99
    }
}