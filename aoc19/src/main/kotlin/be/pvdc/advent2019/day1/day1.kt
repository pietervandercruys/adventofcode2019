import java.io.File
import kotlin.math.floor

fun main(args: Array<String>) {
    totalFuel()
    totalFullFuel()
}

private fun totalFullFuel() {
    var totalFullFuel = 0.0
    File("./aoc19/src/main/kotlin/be/pvdc/advent2019/day1/day1.input.txt").forEachLine { mass ->
        totalFullFuel += calculateFullFuel(mass.toDouble())
    }
    println("Total Full fuel: " + totalFullFuel)
}

private fun totalFuel() {
    var totalFuel = 0.0
    File("./src/be/pvdc/advent2019/day1/day1.input.txt").forEachLine { mass ->
        totalFuel += calculateFuel(mass.toDouble())
    }
    println("Total fuel: " + totalFuel)
}

fun calculateFullFuel(mass: Double): Double {
    val fuel = calculateFuel(mass)
    if(fuel < 1)
        return 0.0;
    return fuel + calculateFullFuel(fuel)
}

fun calculateFuel(mass: Double): Double {
    return floor(mass / 3)-2
}