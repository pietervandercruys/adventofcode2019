package be.pvdc.advent2019.day8

import java.io.File

val wide = 25
val tall = 6
fun main() {
    val layerSize = wide * tall
    var input = readInput()
    val nrOfLayers = input.length / layerSize

    val layers = splitToLayers(nrOfLayers, input, layerSize)

    for (y in 0..tall-1){
        for (x in 0..wide-1){
            print(getPixel(x, y, layers))
        }
        println()
    }
}

private fun splitToLayers(nrOfLayers: Int, input: String, layerSize: Int): MutableList<String> {
    val layers = mutableListOf<String>()
    for (x in 0..nrOfLayers - 1) {
        layers.add(input.substring(layerSize * x, layerSize * (x + 1)))
    }
    return layers
}

private fun getPixel(x: Int, y: Int, layers: List<String>): Char {
    return layers.stream()
            .map { l -> l.get(x + y* wide) }
            .filter{pixel ->
                pixel.equals('1') || pixel.equals('0')
            }
            .map { pixel -> if (pixel.equals('1')) return@map 'x' else return@map ' '}
            .findFirst()
            .get()
}

private fun readInput(): String {
    return File("./aoc19/src/main/kotlin/be/pvdc/advent2019/day8/day8.input.txt").readText(Charsets.UTF_8)
}

private fun countChar(inputString: String, char: Char): Long {
    return inputString.toList().stream()
            .filter({c -> char.equals(c)})
            .count()
}