package be.pvdc.advent2019.day6

import java.io.File
import kotlin.collections.HashMap

val universe = HashMap<String, SpaceObject>();

fun main() {

    indexingTheUniverse()

    val totalNrOfOrbits = universe.values.stream()
            .map { o -> o.nrOfOrbits() }
            .reduce(0, Integer::sum)

    println(totalNrOfOrbits)

    val youObject = getSpaceObject("YOU")
    val sanObject = getSpaceObject("SAN")
    val closestCommonIntersection = findClosestCommonIntersection(youObject, sanObject)

    val intersectionOrbitsToCenter = closestCommonIntersection.nrOfOrbits();
    val youOrbitsToCenter = youObject.nrOfOrbits()
    val sanOrbitsToCenter = sanObject.nrOfOrbits()

    val youOrbitsToIntersection = youOrbitsToCenter - intersectionOrbitsToCenter -1
    val sanOrbitsToIntersection = sanOrbitsToCenter - intersectionOrbitsToCenter -1

    println(youOrbitsToIntersection + sanOrbitsToIntersection)
}

private fun indexingTheUniverse() {
    File("./aoc19/src/main/kotlin/be/pvdc/advent2019/day6/day6.input.txt").forEachLine { orbit ->
        val first = firstObject(orbit)
        val second = secondObject(orbit)

        val firstObject = getSpaceObject(first)
        val secondObject = getSpaceObject(second)

        secondObject.orbits = firstObject
    }
}

private fun findClosestCommonIntersection(spaceObject1: SpaceObject, spaceObject2: SpaceObject): SpaceObject {
    val path1 = spaceObject1.getPathToCenter().reversed();
    val path2 = spaceObject2.getPathToCenter().reversed();
    return path1.stream()
            .filter({o -> path2.contains(o)})
            .findFirst().orElse(null)

}


private fun firstObject(orbit: String): String {
    return orbit.substring(0,orbit.indexOf(')'));
}
private fun secondObject(orbit: String): String {
    return orbit.substring(orbit.indexOf(')')+1);
}

private fun getSpaceObject(objectName: String): SpaceObject {
    var spaceObject = universe.get(objectName)
    if(spaceObject == null) {
        spaceObject = SpaceObject(objectName);
        universe.put(objectName, spaceObject)
    }
    return spaceObject
}

class SpaceObject(val name: String) {
    var orbits: SpaceObject? = null

    fun nrOfOrbits(): Int {
        if (orbits == null) {
            return 0
        }
        else
            return orbits!!.nrOfOrbits() +1
    }

    fun getPathToCenter(): MutableList<SpaceObject> {
        var path: MutableList<SpaceObject>
        if( orbits == null) {
            path = ArrayList<SpaceObject>();
        }
        else {
            path = orbits!!.getPathToCenter()
        }
        path.add(this)
        return path
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SpaceObject

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}
