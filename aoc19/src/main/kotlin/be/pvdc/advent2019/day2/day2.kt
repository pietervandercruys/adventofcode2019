package be.pvdc.advent2019.day2

import java.io.File
import java.lang.Exception

fun main() {
    val textInput = File("./aoc19/src/main/kotlin/be/pvdc/advent2019/day2/day2.input.txt").readText(Charsets.UTF_8)

    for (noun in 0..99) {
        for (verb in 0..99) {
            val input = textInput.split(",").map { it.toInt() }.toIntArray()
            val result = process(input, noun, verb)

            if (noun == 12 && verb == 2) {
                println("the answer is "+result)
            }

            if (result == 19690720) {
                println("the answer is "+(100 * noun + verb))
                return
            }
        }
    }
}

private fun process(input: IntArray, noun: Int, verb: Int): Int {
    var input1 = replace(input, noun, verb)
    var pos = 0;
    while (input1[pos] != 99) {
        input1 = process(input1, pos)
        pos += 4
    }
    val result = input1[0];
    return result
}

fun process(input: IntArray, pos: Int): IntArray {
    val opcode = input[pos]
    val var1 = input[input[pos+1]]
    val var2 = input[input[pos+2]]
    val result = calculate(opcode, var1, var2)
    input[input[pos+3]] = result;
    return input;
}

fun calculate(opcode: Int, var1: Int, var2: Int): Int {
    if(opcode == 1)
        return var1 + var2
    if(opcode == 2)
        return var1 * var2
    throw Exception("Illegal opcode "+opcode)
}

fun replace(input: IntArray, noun: Int, verb: Int): IntArray {
    input[1] = noun
    input[2] = verb
    return input
}
