package be.pvdc.advent2019.day7

class IntcodeComputer(var name: String, var textInput: String, var programInput: IntArray) {
    val output = ArrayList<Int>()
    var inputComputer: IntcodeComputer? = null
        get() = field
        set(inputComputer) {
        field = inputComputer
    }

    fun run(): ArrayList<Int> {
        output.clear()
        val input = textInput.split(",").map { it.toInt() }.toIntArray()
        process(input)
        return output
    }

    private fun process(input: IntArray) {
        var state = State(input, 0)
        while (! state.final()) {
            state = process(state)
        }
    }

    fun hasOutput(): Boolean {
        return output.size > 0
    }

    fun getOutput(): Int {
        return output.removeAt(0)
    }

    var programInputPointer = 0
    private fun readInput(input: IntArray, pos: Int): State {
        val var1 = input[pos+1]

        if(programInputPointer < programInput.size)
        {
            input[var1] = programInput[programInputPointer]
            programInputPointer++
        }
        else {
            while (!inputComputer!!.hasOutput()) {
//                println(name + " is waiting")
                Thread.sleep(10)
            }
            input[var1] = inputComputer!!.getOutput()
        }
        return State(input, pos+2)
    }

    private fun output(input: IntArray, pos: Int): State {
        val var1 = getPar1(input, pos)
//        println(name+" Output "+var1)
        output.add(var1)
        return State(input, pos+2)
    }

    private fun process(state: State): State {
        val input = state.programm
        val pos = state.pos
        val fullOpcode = input[pos]
        val opcode = getOpcode(input, pos)
        if (opcode == 1)
            return add(input, pos)
        if(opcode == 2)
            return multiply(input, pos)
        if(opcode == 3)
            return readInput(input, pos)
        if(opcode == 4)
            return output(input, pos)
        if(opcode == 5)
            return jumpIfTrue(input, pos)
        if(opcode == 6)
            return jumpIfFalse(input, pos)
        if(opcode == 7)
            return lessThan(input, pos)
        if(opcode == 8)
            return equals(input, pos)

        return State(input, pos);
    }

    private fun jumpIfTrue(input: IntArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        if(var1 != 0)
            return State(input, var2)
        else
            return State(input, pos+3)
    }

    private fun jumpIfFalse(input: IntArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        if(var1 == 0)
            return State(input, var2)
        else
            return State(input, pos+3)
    }

    private fun lessThan(input: IntArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val var3 = input[pos+3]
        if(var1 < var2)
            input[var3] = 1
        else
            input[var3] = 0
        return State(input, pos+4)
    }

    private fun equals(input: IntArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val var3 = input[pos+3]
        if(var1 == var2)
            input[var3] = 1
        else
            input[var3] = 0
        return State(input, pos+4)
    }

    private fun add(input: IntArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val result = var1 + var2
        writeResult(opcode, input, pos, result)
        return State(input, pos+4)
    }

    private fun multiply(input: IntArray, pos: Int): State {
        val opcode = input[pos]
        val var1 = getPar1(input, pos)
        val var2 = getPar2(input, pos)
        val result = var1 * var2
        writeResult(opcode, input, pos, result)
        return State(input, pos+4)
    }

    private fun writeResult(opcode: Int, input: IntArray, pos: Int, result: Int) {
        if (par3ImmediateMode(opcode))
            input[pos + 3] = result
        else
            input[input[pos + 3]] = result
    }

    fun getOpcode(input: IntArray, pos: Int): Int {
        return input[pos].rem(10)
    }

    fun getPar1(input: IntArray, pos: Int): Int {
        val opcode = input[pos]
        if(par1ImmediateMode(opcode))
            return input[pos + 1]
        else
            return input[input[pos+1]]
    }

    fun getPar2(input: IntArray, pos: Int): Int {
        val opcode = input[pos]
        if(par2ImmediateMode(opcode))
            return input[pos + 2]
        else
            return input[input[pos+2]]
    }

    fun getPar3(input: IntArray, pos: Int): Int {
        val opcode = input[pos]
        if(par3ImmediateMode(opcode))
            return input[pos + 3]
        else
            return input[input[pos+3]]
    }

    fun replace(input: IntArray, noun: Int, verb: Int): IntArray {
        input[1] = noun
        input[2] = verb
        return input
    }

    private fun par1ImmediateMode(opcode: Int): Boolean {
        return parImmadiateMode(opcode, 1)
    }

    private fun par2ImmediateMode(opcode: Int): Boolean {
        return parImmadiateMode(opcode, 2)
    }

    private fun par3ImmediateMode(opcode: Int): Boolean {
        return parImmadiateMode(opcode, 3)
    }

    private fun parImmadiateMode(opcode: Int, par: Int): Boolean {
        val opcodeString = opcode.toString()
        if(opcodeString.length < 2+par)
            return false
        if(opcodeString[opcodeString.length-(2+par)] == '1')
            return true

        return false
    }
}

private class State(val programm: IntArray, val pos: Int) {
    fun final(): Boolean {
        return programm[pos] == 99
    }
}