package be.pvdc.advent2019.day7

import java.io.File
import java.lang.Integer.max

fun main() {

//    runAmplifiersSequence(intArrayOf(9,7,8,5,6))
    val allSequences = AllPermutation(intArrayOf(5, 6, 7, 8, 9)).getAllPermutations()

    var maxSingnal = 0;

    for (sequence in allSequences) {
        val signal = runAmplifiersSequence(sequence)
        sequence.forEach { s -> print(s)};
        println(" - "+ signal)
        maxSingnal = max(maxSingnal, signal)
    }
    println(maxSingnal)
}

private fun readInput(): String {
    return File("./aoc19/src/main/kotlin/be/pvdc/advent2019/day7/day7.input.txt").readText(Charsets.UTF_8)
//    return "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"
}

private fun runAmplifiersSequence(sequence: IntArray): Int {
    val amplifierA = IntcodeComputer("A", readInput(), intArrayOf(sequence[0], 0))
    val amplifierB = IntcodeComputer("B", readInput(), intArrayOf(sequence[1]))
    val amplifierC = IntcodeComputer("C", readInput(), intArrayOf(sequence[2]))
    val amplifierD = IntcodeComputer("D", readInput(), intArrayOf(sequence[3]))
    val amplifierE = IntcodeComputer("E", readInput(), intArrayOf(sequence[4]))
    amplifierA.inputComputer  = amplifierE
    amplifierB.inputComputer  = amplifierA
    amplifierC.inputComputer  = amplifierB
    amplifierD.inputComputer  = amplifierC
    amplifierE.inputComputer  = amplifierD


    val amplifiers = listOf<IntcodeComputer>(
            amplifierA,
            amplifierB,
            amplifierC,
            amplifierD,
            amplifierE
    )

    amplifiers.parallelStream()
            .forEach({a -> a.run()})

    return amplifierE.output.get(0)
}